# lccat - Librecast Multicast Testing Tool

## Status

In development.  Pre-alpha.

## Build & Install

Dependencies:
- librecast

`make`

`make install`

## Usage

`lccat [-i interface] [-b bridge] [--loop] channel ... [channel]`

The `-b` option will create `bridge` as a virtual bridge if it does not exist,
and create and connect a tap interface to that bridge.

## Questions, Bug reports, Feature Requests

New issues can be raised at:

https://codeberg.org/librecast/lccat/issues

It's okay to raise an issue to ask a question.  You can also email or ask on
IRC.

<hr />

### IRC channel

`#librecast` on libera.chat

If you have a question, please be patient. An answer might take a few hours
depending on time zones and whether anyone on the team is available at that
moment. 

<hr />

## License

This work is dual-licensed under GPL 2.0 and GPL 3.0.

SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

## Funding

This project is funded through [NGI Assure](https://nlnet.nl/assure), a fund established by [NLnet](https://nlnet.nl) with financial support from the European Commission's [Next Generation Internet](https://ngi.eu) program. Learn more at the [NLnet project page](https://nlnet.nl/project/LibreCastLiveStudio).

[<img src="https://nlnet.nl/logo/banner.png" alt="NLnet foundation logo" width="20%" />](https://nlnet.nl)
[<img src="https://nlnet.nl/image/logos/NGIAssure_tag.svg" alt="NGI Assure Logo" width="20%" />](https://nlnet.nl/assure)
