/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2017-2022 Brett Sheffield <bacs@librecast.net> */
#include <arpa/inet.h>
#include <librecast.h>
#include <librecast/if.h>
#include <net/if.h>
#include <signal.h>
#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define BUFLEN 1024

#define LOG_ERROR 0
#define LOG_DEBUG 1
#define LOG_TRACE 2

static int running = 1;
static int verbose;
static int fdtun;
static char *program;
static char *bridge;
static char tapname[IFNAMSIZ];
static char *iface;
static unsigned int ifx;
static lc_socket_t *sock;

static void howtoexit(int signo)
{
	char msg[] = "^D to to exit\n";
	if (signo) if (write(2, "\n", 1) == -1) perror("write");
	if (write(STDERR_FILENO, msg, sizeof msg) == -1) perror("write");
	running = 0;
}

static void logmsg(int level, char *fmt, ...)
{
	if (level > verbose) return;
	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
}

static void *listen_thread(void *arg)
{
	char buf[BUFLEN];
	ssize_t len;
	while (running) {
		len = lc_socket_recv(sock, buf, sizeof buf, 0);
		if (len == -1) {
			perror("lc_socket_recv");
			if (errno == EINTR) break;
		}
		else if (len > 0) {
			if (write(STDOUT_FILENO, buf, len) == -1) perror("write");
		}
	}
	return arg;
}

static int lccat(lc_channel_t *chan[])
{
	char buf[BUFLEN];
	ssize_t len;
	while (running) {
		len = read(STDIN_FILENO, buf, sizeof buf);
		if (len > 0) for (int i = 0; chan[i]; i++) {
			len = lc_channel_send(chan[i], buf, len, 0);
			if (len == -1) perror("lc_channel_send()");
			else if (len > 0) logmsg(LOG_DEBUG, "%zi bytes sent\n", len);
		}
		else break;
	}
	return 0;
}

static int usage(int rc)
{
	logmsg(LOG_ERROR, "usage: %s [-i interface] [--loop] channel ... [channel]\n", program);
	return rc;
}

static int set_interface(char *arg)
{
	ifx = if_nametoindex(arg);
	if (!ifx) {
		logmsg(LOG_ERROR, "invalid interface\n");
		return -1;
	}
	iface = arg;
	return 0;
}

static int process_args(lc_ctx_t *ctx, int argc, char *argv[], lc_channel_t *chan[])
{
	int c = 0;
	int err;
	if (!(sock = lc_socket_new(ctx))) {
		perror("lc_socket_new");
		return -1;
	}
	for (int i = 0; i < argc; i++) {
		if (!strcmp(argv[i], "-i")) {
			if (argv[i + 1]) {
				if (set_interface(argv[i + 1]) == -1) {
					return -1;
				}
				if (lc_socket_bind(sock, ifx) == -1) {
					perror("lc_socket_bind");
					return -1;
				}
				i++;
			}
			else return usage(1);
		}
		else if (!strcmp(argv[i], "-v")) {
			verbose++;
		}
		else if (!strcmp(argv[i], "--taponly")) {
			memset(tapname, 0, sizeof tapname);
			fdtun = lc_tap_create(tapname);
			if (fdtun == -1) {
				perror("lc_tuntap_create");
				return -1;
			}
			lc_link_set(ctx, tapname, 1);
			if (set_interface(tapname) == -1) {
				return -1;
			}
			if (lc_socket_bind(sock, ifx) == -1) {
				perror("lc_socket_bind");
				return -1;
			}
			fprintf(stderr, "created tap '%s'\n", tapname);
		}
		else if (!strcmp(argv[i], "-b") || !strcmp(argv[i], "--bridge")) {
			if (argv[i + 1]) {
				i++;
				bridge = argv[i];
				fprintf(stderr, "bridge: %s\n", bridge);
				memset(tapname, 0, sizeof tapname);
				fdtun = lc_tap_create(tapname);
				if (fdtun == -1) {
					perror("lc_tuntap_create");
					return -1;
				}
				if ((err = lc_bridge_addif(ctx, bridge, tapname))) {
					fprintf(stderr, "lc_bridge_add: %s\n", strerror(err));
					return -1;
				}
				lc_link_set(ctx, tapname, 1);
				if (set_interface(tapname) == -1) {
					return -1;
				}
				if (lc_socket_bind(sock, ifx) == -1) {
					perror("lc_socket_bind");
					return -1;
				}
				fprintf(stderr, "created tap '%s'\n", tapname);
			}
			else return -1;
		}
		else if (!strcmp(argv[i], "--loop")) {
			if (lc_socket_loop(sock, 1) == -1) {
				perror("lc_socket_loop");
				return -1;
			}
		}
		else {
			printf("adding channel '%s'\n", argv[i]);
			struct sockaddr_in6 sa = {
				.sin6_family = AF_INET6,
				.sin6_port = htons(4242)
			};
			inet_pton(AF_INET6, argv[i], &sa.sin6_addr);
			if (IN6_IS_ADDR_MULTICAST(&sa.sin6_addr)) {
				fprintf(stderr, "Using multicast address %s\n", argv[i]);
				if (!(chan[c] = lc_channel_init(ctx, &sa))) {
					perror("lc_channel_init");
					return -1;
				}
			}
			else {
				if (!(chan[c] = lc_channel_new(ctx, argv[i]))) {
					perror("lc_channel_new");
					return -1;
				}
			}
			if (lc_channel_bind(sock, chan[c]) == -1) {
				perror("lc_channel_bind");
				return -1;
			}
			if (lc_channel_join(chan[c]) == -1) {
				perror("lc_channel_join");
				return -1;
			}
			c++;
		}
	}
	return (c > 0) ? 0 : -1;
}

int main(int argc, char *argv[])
{
	lc_ctx_t *ctx;
	lc_channel_t *chan[argc];
	pthread_t tid_in;
	int rc = 0;

	program = argv[0];
	if (argc < 2) {
		return usage(0);
	}
	memset(chan, 0, sizeof(lc_channel_t *) * argc);
	ctx = lc_ctx_new();
	if ((rc = process_args(ctx, argc - 1, &argv[1], chan))) {
		usage(1);
		goto exit_0;
	}
	pthread_create(&tid_in, NULL, &listen_thread, NULL);

	signal(SIGINT, &howtoexit);
	howtoexit(0);
	running = 1;
	rc = lccat(chan);

	pthread_cancel(tid_in);
	pthread_join(tid_in, NULL);
exit_0:
	lc_ctx_free(ctx);

	return rc;
}
